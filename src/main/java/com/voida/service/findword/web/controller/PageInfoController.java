package com.voida.service.findword.web.controller;

import com.voida.logging.*;
import com.voida.service.findword.service.PageInfoService;
import com.voida.service.findword.web.dto.PageInfoDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static com.voida.service.findword.web.controller.MappingConstants.PAGE_INFO_URL;

@RestController
@RequiredArgsConstructor
public class PageInfoController {
    private final PageInfoService pageInfoService;

    @GetMapping(PAGE_INFO_URL)
    @Audited(uniqueId = @UniqueId(create = CreateStrategy.IF_NOT_EXISTS, remove = RemoveStrategy.RETURN_VALUE), policy = LoggingPolicy.ALL)
    public PageInfoDTO getPageInfo(@RequestParam(name = "pageId") Integer pageId,
                                   @RequestParam(name = "languageId", required = false) Integer languageId) {
        return pageInfoService.getByPageIdAndLanguageId(pageId, languageId);
    }

}
