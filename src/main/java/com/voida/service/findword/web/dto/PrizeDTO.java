package com.voida.service.findword.web.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
public class PrizeDTO {
    private Integer id;
    private String giftKey;
    private String icon;
    private BigDecimal price;
    private String type;
    private String name;
    private String description;
}
