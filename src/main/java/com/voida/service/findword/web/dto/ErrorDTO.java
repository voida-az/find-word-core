package com.voida.service.findword.web.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class ErrorDTO {
    private Integer code;
    private String status;
    private String message;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer customErrorCode;
}
