package com.voida.service.findword.web.dto;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class StatisticsDTO {
    private Integer gamesPlayed;
    private Integer activeGames;
    private Integer finishedGames;

    private Integer questionPlayed;
    private Integer questionsWon;

    private Integer questionsPlayedToday;
    private Integer questionWonToday;

    private Long score;
    private Integer leaderBoardRank;
}
