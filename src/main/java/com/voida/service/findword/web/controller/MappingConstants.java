package com.voida.service.findword.web.controller;

public class MappingConstants {
    public static final String VERIFY_TOKEN_URL = "/verify";
    public static final String QUESTION_URL = "/question";
    public static final String ANSWER_URL = "/answer";
    public static final String LEADER_BOARD_URL = "/leaderBoard";
    public static final String STATISTICS_URL = "/statistics";
    public static final String HISTORY_URL = "/history";
    public static final String PAGE_INFO_URL = "/pageInfo";
    public static final String EXTRA_GAME_URL = "/extraGame";
    public static final String REFRESH_DATA_URL = "/refresh";
    public static final String PRIZE_URL = "/prizes";
    public static final String HINT_URL = "/hint";
    public static final String MATH_QUESTION_GENERATION_URL = "/generateQuestions";

    public static final String HEADER_STRING = "authorization";

}
