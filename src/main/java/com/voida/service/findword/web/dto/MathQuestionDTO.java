package com.voida.service.findword.web.dto;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class MathQuestionDTO {
    private Integer answer;
    private StringBuilder solution;
    private List<Integer> numbers = new ArrayList<>();
}
