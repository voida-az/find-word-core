package com.voida.service.findword.web.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DataRefreshDTO {
    private String value;
}
