package com.voida.service.findword.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class LeaderScore {
    private Long accountId;
    private Long points;
    private Long spentTime;
}
