package com.voida.service.findword.web.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PageInfoDTO {
    private String title;

    @ToString.Exclude
    private String text;
}
