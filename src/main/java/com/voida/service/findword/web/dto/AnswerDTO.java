package com.voida.service.findword.web.dto;

import com.voida.service.findword.validation.AnswerConstraint;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
public class AnswerDTO {

    @AnswerConstraint
    private String answer;

    @NotEmpty
    private String appKeyword;

    @NotNull
    @Min(0)
    private Long spentTime;
}
