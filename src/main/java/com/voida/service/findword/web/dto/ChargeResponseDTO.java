package com.voida.service.findword.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class ChargeResponseDTO {
    private Boolean success;
    private String message;
}
