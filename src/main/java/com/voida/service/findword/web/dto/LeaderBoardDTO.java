package com.voida.service.findword.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class LeaderBoardDTO {
    List<UserScoreDTO> topUsers;
    UserScoreDTO userScore;
}
