package com.voida.service.findword.persistence.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
public class UserGame {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date", nullable = false)
    private Date date;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expiration_date", nullable = false)
    private Date expirationDate;

    @Column(name = "points")
    private Integer points;

    @Column(name = "questions_played")
    private Integer questionPlayed;

    @Column(name = "hints_played")
    private Integer hintsPlayed;

    @Column(name = "question_num")
    private Integer questionNum;

    @OneToMany(mappedBy = "userGame")
    private List<UserQuestion> userQuestionList;

    @Column(name = "spentTime")
    private Long spentTime;

}
