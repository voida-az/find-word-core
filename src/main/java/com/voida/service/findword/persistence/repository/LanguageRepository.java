package com.voida.service.findword.persistence.repository;

import com.voida.service.findword.persistence.model.entity.Language;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LanguageRepository extends JpaRepository<Language, Integer> {
    Optional<Language> findByIsDefaultIsTrue();
}
