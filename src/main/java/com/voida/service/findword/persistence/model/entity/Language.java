package com.voida.service.findword.persistence.model.entity;

import com.voida.service.findword.persistence.model.audit.Audit;
import com.voida.service.findword.persistence.model.audit.AuditListener;
import com.voida.service.findword.persistence.model.audit.Auditable;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@EntityListeners(AuditListener.class)
public class Language implements Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "code", nullable = false, columnDefinition = "NVARCHAR(20)", unique = true)
    private String code;

    @Column(name = "is_default", columnDefinition = "bit default 0")
    private Boolean isDefault;

    @Embedded
    private Audit audit;
}
