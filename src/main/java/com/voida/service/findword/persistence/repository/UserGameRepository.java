package com.voida.service.findword.persistence.repository;

import com.voida.service.findword.persistence.model.entity.UserGame;
import com.voida.service.findword.web.dto.LeaderScore;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface UserGameRepository extends JpaRepository<UserGame, Integer> {

    @Query("SELECT ug " +
            "FROM UserGame ug " +
            "WHERE ug.user.accountId = ?1 " +
            "ORDER BY ug.date DESC")
    List<UserGame> findByAccountId(Long accountId);

    @Query("SELECT ug " +
            "FROM UserGame ug " +
            "WHERE ug.user.accountId = ?1 " +
            "ORDER BY ug.date DESC")
    List<UserGame> findByAccountIdAndPage(Pageable pageable, Long accountId);

    @Query("SELECT g " +
            "FROM UserGame g " +
            "WHERE g.user.accountId = ?1 " +
            "AND g.date > ?2 " +
            "ORDER BY g.date DESC")
    List<UserGame> findLastByAccountId(Pageable pageable, Long accountId, Date date);

    // findLeadersByPeriod causes warning: domain type or valid projection interface expected here
    // removing By helps, but figure out why
    @Query("SELECT new com.voida.service.findword.web.dto.LeaderScore(ug.user.accountId, SUM(ug.points), SUM(ug.spentTime)) " +
            "FROM UserGame ug " +
            "WHERE ug.date > ?1 " +
            "GROUP BY ug.user.id " +
            "ORDER BY SUM(ug.points) DESC, SUM(ug.spentTime)")
    List<LeaderScore> findLeadersInPeriod(Pageable pageable, Date date);

    @Query("SELECT new com.voida.service.findword.web.dto.LeaderScore(ug.user.accountId, SUM(ug.points), SUM(ug.spentTime)) " +
            "FROM UserGame ug " +
            "JOIN User u " +
            "ON ug.user.id = u.id " +
            "WHERE ug.date > ?2 " +
            "AND u.accountId = ?1")
    LeaderScore findUserPointsInPeriod(Long accountId, Date startDate);

    @Query("SELECT count(ug) " +
            "FROM UserGame ug " +
            "WHERE ug.date > ?3 " +
            "AND ug.user.accountId <> ?1 " +
            "GROUP BY ug.user.id " +
            "HAVING SUM(ug.points) > ?2 OR (SUM(ug.points) = ?2 AND SUM(ug.spentTime) < ?4)")
    List<Integer> findUserPosition(Long accountId, Long points, Date startDate, Long spentTime);
}
