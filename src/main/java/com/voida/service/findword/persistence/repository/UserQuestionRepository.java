package com.voida.service.findword.persistence.repository;

import com.voida.service.findword.persistence.model.entity.UserQuestion;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface UserQuestionRepository extends JpaRepository<UserQuestion, Integer> {

    @EntityGraph(attributePaths = {"question", "userGame", "userGame.user"})
    @Query("SELECT uq " +
            "FROM UserQuestion uq " +
            "JOIN uq.userGame g " +
            "WHERE g.user.accountId = ?2 " +
            "AND uq.date > ?1 " +
            "ORDER BY uq.date DESC")
    List<UserQuestion> findLastByAccountId(Pageable pageable, Date date, Long accountId);

    @EntityGraph(attributePaths = {"userGame", "userGame.user"})
    @Query("SELECT uq " +
            "FROM UserQuestion uq " +
            "JOIN uq.userGame g " +
            "WHERE g.user.id = ?2 " +
            "AND uq.date > ?1")
    List<UserQuestion> findByDateAndUserId(Date date, Integer userId);

    @EntityGraph(attributePaths = {"userGame", "userGame.user"})
    @Query("SELECT uq " +
            "FROM UserQuestion uq " +
            "JOIN uq.userGame g " +
            "WHERE g.user.accountId = ?1")
    List<UserQuestion> findByAccountId(Long accountId);

//    // TODO: think about case when user has multiple unanswered questions in 1 minute
//    @Modifying
//    @Query(nativeQuery = true,
//            value = "update user_game ug " +
//                    "join user_question uq " +
//                    "on ug.id = uq.user_game_id " +
//                    "set ug.spent_time = ug.spent_time + ?3, " +
//                    "    uq.spent_time = ?3 " +
//                    "where uq.date > ?1 " +
//                    "and uq.date <= ?2 " +
//                    "and uq.answer is null")
//    void updateUnansweredQuestions(Date startDate, Date endDate, Long spentTime);
}
