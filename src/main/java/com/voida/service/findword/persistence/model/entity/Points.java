package com.voida.service.findword.persistence.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class Points {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "answer_diff", nullable = false, unique = true)
    private Integer answerDiff; // missing_letter_num for word_game, +- number for math_game

    @Column(name = "points")
    private Integer points;
}
