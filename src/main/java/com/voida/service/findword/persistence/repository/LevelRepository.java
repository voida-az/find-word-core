package com.voida.service.findword.persistence.repository;

import com.voida.service.findword.persistence.model.entity.Level;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface LevelRepository extends JpaRepository<Level, Integer> {

    Optional<Level> findByLevel(Integer level);
}
