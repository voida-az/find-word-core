package com.voida.service.findword.persistence.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
public class UserQuestion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_game_id", nullable = false)
    private UserGame userGame;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "question_id", nullable = false)
    private Question question;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date")
    private Date date;

    @Column(name = "answer", columnDefinition = "NVARCHAR(50)")
    private String answer;

    @Column(name = "points")
    private Integer points;

    @Column(name = "spentTime")
    private Long spentTime;

}
