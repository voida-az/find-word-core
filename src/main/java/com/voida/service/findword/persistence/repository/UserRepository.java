package com.voida.service.findword.persistence.repository;

import com.voida.service.findword.persistence.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
    User getByAccountId(Long accountId);
}
