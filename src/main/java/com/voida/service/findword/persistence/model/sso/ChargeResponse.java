package com.voida.service.findword.persistence.model.sso;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ChargeResponse {
    private Integer code;
    private String message;
    private String status;
}
