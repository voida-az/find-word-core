package com.voida.service.findword.persistence.repository;

import com.voida.service.findword.persistence.model.entity.PageInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface PageInfoRepository extends JpaRepository<PageInfo, Integer> {

    @Query("SELECT pi " +
            "FROM PageInfo pi " +
            "WHERE pi.language.id = ?2 " +
            "AND pi.pageId = ?1")
    Optional<PageInfo> findByPageIdAndLanguageId(Integer pageId, Integer languageId);
}
