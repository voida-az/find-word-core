package com.voida.service.findword.persistence.repository;

import com.voida.service.findword.persistence.model.entity.UserScore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface UserScoreRepository extends JpaRepository<UserScore, Integer> {

    @Query("SELECT us " +
            "FROM UserScore us " +
            "JOIN us.user u " +
            "WHERE u.accountId = ?1")
    Optional<UserScore> findByAccountId(Long accountId);

    @Query("SELECT count(us) " +
            "FROM UserScore us " +
            "WHERE us.points > ?1")
    Integer findUserPosition(Long userPoints);

    @Modifying
    @Query("UPDATE UserScore us " +
            "SET us.points = us.points + ?2 " +
            "WHERE us.user.id = ?1")
    void updateUserScore(Integer userId, Long points);

}
