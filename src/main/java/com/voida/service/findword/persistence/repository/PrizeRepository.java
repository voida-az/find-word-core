package com.voida.service.findword.persistence.repository;

import com.voida.service.findword.persistence.model.entity.Prize;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PrizeRepository extends JpaRepository<Prize, Integer> {

    @Query("SELECT p " +
            "FROM Prize p " +
            "WHERE p.period.id = ?1")
    List<Prize> findByPeriodId(Integer periodId);
}