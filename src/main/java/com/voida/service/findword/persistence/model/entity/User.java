package com.voida.service.findword.persistence.model.entity;

import com.voida.service.findword.persistence.model.audit.Audit;
import com.voida.service.findword.persistence.model.audit.AuditListener;
import com.voida.service.findword.persistence.model.audit.Auditable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@EntityListeners(AuditListener.class)
public class User implements Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id; //FIXME: should be long

    @Column(name = "account_id", unique = true, nullable = false)
    private Long accountId;

    @OneToMany(mappedBy = "user")
    private List<UserGame> userGameList;

    @Embedded
    private Audit audit;

}
