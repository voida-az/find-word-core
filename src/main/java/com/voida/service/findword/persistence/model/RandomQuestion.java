package com.voida.service.findword.persistence.model;

import com.voida.service.findword.persistence.model.entity.Question;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class RandomQuestion {
    private Question question;
    private String formattedQuestion;
    private String answer;
}
