package com.voida.service.findword.persistence.repository;

import com.voida.service.findword.persistence.model.entity.Question;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface QuestionRepository extends JpaRepository<Question, Integer> {

    List<Question> findByQuestion(String question); // TODO: should return Word not List when unique constraint is added

    @Query("SELECT w " +
            "FROM Question w " +
            "LEFT JOIN w.level l " +
            "WHERE l.level = ?1")
    List<Question> findAllByLevel(Integer level);

    @EntityGraph(attributePaths = {"topic"})
    @Query("SELECT w " +
            "FROM Question w " +
            "LEFT JOIN w.level l " +
            "WHERE l.level = ?1 " +
            "AND w.language.id = ?2")
    List<Question> findAllByLevelAndLanguageId(Integer level, Integer languageId);
}
