package com.voida.service.findword.persistence.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class AnswerResult {
    private Integer answerDiff;
    private Boolean isValid;
    private String correctAnswer;
}
