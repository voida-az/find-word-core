package com.voida.service.findword.persistence.model.audit;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

public class AuditListener {

    @PrePersist
    public void setCreatedOn(Object obj) {
        Audit audit = getAudit(obj);
        audit.setCreatedOn(LocalDateTime.now());
    }

    @PreUpdate
    public void setUpdatedOn(Object obj) {
        Audit audit = getAudit(obj);
        audit.setUpdatedOn(LocalDateTime.now());
    }

    private Audit getAudit(Object obj) {
        Auditable auditable = (Auditable)obj;
        Audit audit = auditable.getAudit();
        if(audit == null) {
            audit = new Audit();
            auditable.setAudit(audit);
        }
        return audit;
    }
}
