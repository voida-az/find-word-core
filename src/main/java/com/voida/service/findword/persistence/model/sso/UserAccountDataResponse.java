package com.voida.service.findword.persistence.model.sso;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
public class UserAccountDataResponse {
    private Integer code;
    private String message;
    private String error;
    private List<UserAccountData> accounts;

    @Getter
    @Setter
    @ToString
    public static class UserAccountData {
        private Long id;
        private String username;
        private String avatar;
    }
}