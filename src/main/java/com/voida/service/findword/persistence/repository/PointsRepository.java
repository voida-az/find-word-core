package com.voida.service.findword.persistence.repository;

import com.voida.service.findword.persistence.model.entity.Points;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PointsRepository extends JpaRepository<Points, Integer> {
    Points findByAnswerDiff(Integer diff);
}
