package com.voida.service.findword.persistence.model.sso;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
public class VerifyTokenResponse {
    private Integer code;
    private String message;
    private Account account;

    @Getter
    @Setter
    @ToString
    public static class Account {
        private Long id;
        private String username;
    }
}
