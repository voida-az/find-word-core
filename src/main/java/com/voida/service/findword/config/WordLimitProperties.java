package com.voida.service.findword.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ConfigurationProperties(prefix = "word.limits")
@Setter
@Getter
@Profile({"wordGameDev", "wordGameTest", "wordGameProd"})
public class WordLimitProperties {
    private Integer minLength;
    private Integer maxLength;
//    private Integer numPerGame;
//    private Integer hintsPerGame;
}
