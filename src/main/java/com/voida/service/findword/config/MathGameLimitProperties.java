package com.voida.service.findword.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@ConfigurationProperties(prefix = "math.limits")
@Setter
@Getter
@Profile({"mathGameDev", "mathGameTest", "mathGameProd"})
public class MathGameLimitProperties {
    private Integer minValue;
    private Integer maxValue;
    private Integer numPerQuestion;
    private Integer questionsNum;
}
