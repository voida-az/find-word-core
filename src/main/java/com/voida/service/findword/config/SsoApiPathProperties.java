package com.voida.service.findword.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "sso.api.path")
@Setter
@Getter
public class SsoApiPathProperties {
    private String verifyToken;
    private String userAccountData;
    private String directCharging;
}
