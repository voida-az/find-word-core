package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.AnswerResult;
import com.voida.service.findword.persistence.model.entity.Question;
import com.voida.service.findword.persistence.model.RandomQuestion;
import com.voida.service.findword.persistence.model.entity.UserQuestion;

import java.util.List;

public interface QuestionService {
    List<Question> getQuestions(Integer languageId, Integer level);
    RandomQuestion getRandomQuestion(List<Question> questions, List<Integer> userQuestionIds);
    AnswerResult checkAnswer(UserQuestion userQuestion, String userAnswer);
}
