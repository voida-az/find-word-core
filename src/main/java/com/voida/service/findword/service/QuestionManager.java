package com.voida.service.findword.service;

import com.voida.service.findword.web.dto.HintDTO;
import com.voida.service.findword.web.dto.QuestionDTO;
import com.voida.service.findword.web.dto.AnswerResultDTO;

public interface QuestionManager {
    QuestionDTO getQuestion(Long accountId, Integer languageId);
    AnswerResultDTO checkAnswer(Long accountId, String answer, Long spentTime);
    HintDTO getHint(Long accountId);
}
