package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class PeriodNotFoundException extends RuntimeException {
    public PeriodNotFoundException(String s) {
        super(s);
    }
}
