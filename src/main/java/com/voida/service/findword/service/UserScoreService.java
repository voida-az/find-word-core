package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.entity.User;
import com.voida.service.findword.persistence.model.entity.UserScore;

public interface UserScoreService {
    UserScore getUserScore(Long accountId);
    void updateUserScore(Integer userId, Integer points);
    void create(User user);
}
