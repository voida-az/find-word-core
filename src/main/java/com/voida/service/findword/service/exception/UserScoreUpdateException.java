package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class UserScoreUpdateException extends RuntimeException {
    public UserScoreUpdateException(String message) {
        super(message);
    }
}
