package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class UserScoreCreationException extends RuntimeException {
    public UserScoreCreationException(String message) {
        super(message);
    }
}
