package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.sso.UserAccountDataResponse;
import com.voida.service.findword.persistence.model.sso.VerifyTokenResponse;
import com.voida.service.findword.web.dto.ChargeResponseDTO;

import java.util.List;

public interface SsoIntegrationService {
    VerifyTokenResponse verifyToken(String keyword, String token);
    ChargeResponseDTO directCharge(String keyword, Long accountId);
    List<UserAccountDataResponse.UserAccountData> getUserAccounts(List<Long> accountIdList, String appKeyword);
}
