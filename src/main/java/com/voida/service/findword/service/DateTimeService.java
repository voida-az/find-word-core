package com.voida.service.findword.service;

import java.util.Date;

public interface DateTimeService {
    Date getStartOfDay();
    Date getStartOfWeek();
    Date getStartOfMonth();

    String formatDate(Date date);
    Date getEndOfDay();
}
