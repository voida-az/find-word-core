package com.voida.service.findword.service;

import com.voida.service.findword.web.dto.PageInfoDTO;

public interface PageInfoService {
    PageInfoDTO getByPageIdAndLanguageId(Integer pageId, Integer languageId);
}
