package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class TokenVerificationException extends CustomException {
    public TokenVerificationException(Integer code, String message) {
        super(code, message);
    }
}
