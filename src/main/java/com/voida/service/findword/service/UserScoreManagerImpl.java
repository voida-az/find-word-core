package com.voida.service.findword.service;

import com.voida.service.findword.config.LimitProperties;
import com.voida.service.findword.persistence.model.entity.PeriodTypes;
import com.voida.service.findword.persistence.model.entity.UserGame;
import com.voida.service.findword.persistence.model.entity.UserQuestion;
import com.voida.service.findword.persistence.model.entity.UserScore;
import com.voida.service.findword.persistence.model.sso.UserAccountDataResponse;
import com.voida.service.findword.persistence.repository.UserScoreRepository;
import com.voida.service.findword.service.exception.PeriodNotFoundException;
import com.voida.service.findword.service.exception.UserAccountDataException;
import com.voida.service.findword.web.dto.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserScoreManagerImpl implements UserScoreManager {
    private final UserScoreRepository userScoreRepository;
    private final UserScoreService userScoreService;
    private final UserGameService userGameService;
    private final UserQuestionService userQuestionService;
    private final UserService userService;
    private final SsoIntegrationService ssoIntegrationService;
    private final DateTimeService dateTimeService;
    private final LimitProperties limitProperties;

    // TODO: change this method! very bad performance, just temp solution
    // FIXME: periodId 4 was for all time, but it's not needed now
    private UserScoreDTO getUserScoreDTO(Long accountId, UserAccountDataResponse.UserAccountData accountData, Integer periodId) {
//        Long points;
        int userPosition;
//        if(periodId == 4) {
//            points = userScoreService.getUserScore(accountId).getPoints();
//            userPosition = userScoreRepository.findUserPosition(points) + 1;
//        } else {
        Date startDate;
        if(periodId == 1) startDate = dateTimeService.getStartOfDay();
        else if(periodId == 2) startDate = dateTimeService.getStartOfWeek();
        else startDate = dateTimeService.getStartOfMonth();

        LeaderScore leaderScore = userGameService.getUserPoints(accountId, startDate);
        userPosition = userGameService.getUserPosition(accountId, leaderScore.getPoints(), startDate, leaderScore.getSpentTime()) + 1;
//        }

        return new UserScoreDTO(leaderScore.getPoints(), userPosition, getSpentTime(leaderScore.getSpentTime()),
                accountData.getUsername(), accountData.getAvatar());
    }

    private List<LeaderScore> getTopUsers(Integer periodId) {
        if(periodId.equals(PeriodTypes.DAILY.getValue()))
            return userGameService.findLeadersByPeriod(limitProperties.getLeaderboardUserNum(), dateTimeService.getStartOfDay());
        if(periodId.equals(PeriodTypes.WEEKLY.getValue()))
            return userGameService.findLeadersByPeriod(limitProperties.getLeaderboardUserNum(), dateTimeService.getStartOfWeek());
        if(periodId.equals(PeriodTypes.MONTHLY.getValue()))
            return userGameService.findLeadersByPeriod(limitProperties.getLeaderboardUserNum(), dateTimeService.getStartOfMonth());
//        if(periodId.equals(PeriodTypes.OVERALL.getValue())) {
//            List<UserScore> userScores = userScoreRepository.findLeaders(PageRequest.of(0, limitProperties.getLeaderboardUserNum()));
//            List<LeaderScore> topUsers = new ArrayList<>();
//            for(UserScore userScore: userScores) {
//                topUsers.add(new LeaderScore(userScore.getUser().getAccountId(), userScore.getPoints()));
//            }
//            return topUsers;
//        }
        throw new PeriodNotFoundException("Leaderboard period for id: " + periodId + " was not found.");
    }

    private Map<Long, List<UserAccountDataResponse.UserAccountData>> getUserAccounts(
                        List<LeaderScore> topUsers, Long accountId, String appKeyword) {
        List<Long> accountIds = topUsers
                .stream()
                .map(LeaderScore::getAccountId)
                .collect(Collectors.toList());
        if(!accountIds.contains(accountId)) accountIds.add(accountId);

        List<UserAccountDataResponse.UserAccountData> userAccounts = ssoIntegrationService.getUserAccounts(accountIds, appKeyword);
        return userAccounts
                .stream()
                .collect(Collectors.groupingBy(UserAccountDataResponse.UserAccountData::getId));
    }

    private LeaderBoardDTO getUserScores(List<LeaderScore> topUsers, Map<Long,
            List<UserAccountDataResponse.UserAccountData>> userAccountMap, Long accountId) {
        List<UserScoreDTO> userScoreDTOS = new ArrayList<>();
        UserScoreDTO userScoreDTO = null;

        for(int i = 0; i < limitProperties.getLeaderboardUserNum() && i < topUsers.size(); i++) {
            try {
                UserAccountDataResponse.UserAccountData accountData = userAccountMap.get(topUsers.get(i).getAccountId()).get(0);
                UserScoreDTO userScore = new UserScoreDTO(topUsers.get(i).getPoints(), i + 1,
                        getSpentTime(topUsers.get(i).getSpentTime()), accountData.getUsername(), accountData.getAvatar());
                userScoreDTOS.add(userScore);
                if(topUsers.get(i).getAccountId().equals(accountId)) userScoreDTO = userScore;
            } catch(Exception e) {
                throw new UserAccountDataException("Couldn't add user with accountId: " +
                        topUsers.get(i).getAccountId() + " to leaderboard.");
            }
        }
        return new LeaderBoardDTO(userScoreDTOS, userScoreDTO);
    }

    @Override
    public LeaderBoardDTO getLeaderBoard(Long accountId, String appKeyword, Integer periodId) {
        userService.createIfNotExists(accountId); // FIXME: done quickly, change this in statistics and history too!
        List<LeaderScore> topUsers = getTopUsers(periodId);
        Map<Long, List<UserAccountDataResponse.UserAccountData>> userAccountMap = getUserAccounts(topUsers, accountId, appKeyword);

        LeaderBoardDTO leaderBoardDTO = getUserScores(topUsers, userAccountMap, accountId);
        List<UserScoreDTO> userScoreDTOS = leaderBoardDTO.getTopUsers();
        UserScoreDTO userScoreDTO = leaderBoardDTO.getUserScore();

        if(userScoreDTO == null) {
            userScoreDTO = getUserScoreDTO(accountId, userAccountMap.get(accountId).get(0), periodId);
            if(userScoreDTOS.size() < limitProperties.getLeaderboardUserNum()) userScoreDTOS.add(userScoreDTO);
        } else {
            userScoreDTO.setAvatar(userAccountMap.get(accountId).get(0).getAvatar());
            userScoreDTO.setUsername(userAccountMap.get(accountId).get(0).getUsername());
        }
        return new LeaderBoardDTO(userScoreDTOS, userScoreDTO);
    }

    private String getSpentTime(Long spentTime) {
        long minutes = spentTime / 60;
        long seconds = spentTime % 60;
        return String.format("%02d:%02d", minutes, seconds);
    }

    private int getWonQuestions(List<UserQuestion> userQuestions) {
        int questionsWon = 0;
        for(UserQuestion userQuestion : userQuestions) {
            if(userQuestion.getPoints() != null && userQuestion.getPoints() > 0) {
                questionsWon++;
            }
        }
        return questionsWon;
    }

    private int getActiveGames(List<UserGame> userGames) {
        int activeGames = 0;
        for(UserGame userGame : userGames) {
            if(!userGame.getQuestionNum().equals(userGame.getQuestionPlayed())) {
                activeGames++;
            }
        }
        return activeGames;
    }

    @Override
    public StatisticsDTO getStatistics(Long accountId) {
        userService.createIfNotExists(accountId); // create user if not exists
        List<UserGame> userGames = userGameService.getByAccountId(accountId);
        List<UserQuestion> userQuestions = userQuestionService.getByAccountId(accountId);
        UserScore userScore = userScoreService.getUserScore(accountId); //TODO: 3 queries executing!
        Integer userPosition = userScoreRepository.findUserPosition(userScore.getPoints()) + 1;
        int activeGames = getActiveGames(userGames);

        int questionsPlayedToday = 0, questionsWonToday = 0;
        for(UserQuestion userQuestion: userQuestions) {
            if(userQuestion.getDate().after(dateTimeService.getStartOfDay())) {
                questionsPlayedToday++;
                if(userQuestion.getPoints() > 0) questionsWonToday++;
            }
        }

        return new StatisticsDTO(userGames.size(), activeGames, userGames.size() - activeGames, userQuestions.size(),
                getWonQuestions(userQuestions), questionsPlayedToday, questionsWonToday, userScore.getPoints(), userPosition);
    }

    @Override
    public HistoryDTO getHistory(Long accountId, Integer page) {
        userService.createIfNotExists(accountId); // create user if not exists
        List<UserGame> userGames = userGameService.getByAccountIdAndPage(accountId, page);
        List<GameDTO> games = new ArrayList<>();
        for(UserGame userGame: userGames) {
            games.add(userGameService.getGameDTO(userGame));
        }
        return new HistoryDTO(games);
    }

}
