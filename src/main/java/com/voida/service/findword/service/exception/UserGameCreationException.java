package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class UserGameCreationException extends RuntimeException {
    public UserGameCreationException(String message) {
        super(message);
    }
}
