package com.voida.service.findword.service;

import com.voida.service.findword.web.dto.ExtraGameDTO;

public interface SubscriptionService {
    ExtraGameDTO getExtraGame(String keyword, Long accountId);
}
