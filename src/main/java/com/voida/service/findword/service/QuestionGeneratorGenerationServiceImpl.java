package com.voida.service.findword.service;

import com.voida.service.findword.config.MathGameLimitProperties;
import com.voida.service.findword.persistence.model.entity.Level;
import com.voida.service.findword.persistence.model.entity.Question;
import com.voida.service.findword.service.exception.QuestionCreationException;
import com.voida.service.findword.web.dto.ApiResponseDTO;
import com.voida.service.findword.web.dto.MathQuestionDTO;
import com.voida.service.findword.web.dto.QuestionDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Log4j2
@Service
@RequiredArgsConstructor
@Profile({"mathGameDev", "mathGameTest", "mathGameProd"})
public class QuestionGeneratorGenerationServiceImpl implements QuestionGenerationService {
    private final MathGameLimitProperties mathGameLimitProperties;
    private final QuestionDAO questionService;
    private final LevelService levelService;
    private final Random random = new Random();

    private Integer getRandomInteger(List<Integer> chosenNums) {
        int rand = random.nextInt(mathGameLimitProperties.getMaxValue() + 1
                - mathGameLimitProperties.getMinValue()) + mathGameLimitProperties.getMinValue();
        while (chosenNums.contains(rand)) {
            rand = random.nextInt(mathGameLimitProperties.getMaxValue() + 1
                    - mathGameLimitProperties.getMinValue()) + mathGameLimitProperties.getMinValue();
        }
        return rand;
    }

    private Character getRandomOperator() {
        int rand = random.nextInt(4);
        switch (rand) {
            case 0:
                return '+';
            case 1:
                return '-';
            case 2:
                return '*';
            case 3:
                return '/';
            default:
                throw new QuestionCreationException("Operator for random number: " + rand + " was not found.");
        }
    }

    private Integer performOperation(Character op, Integer a, Integer b) {
        switch (op) {
            case '+':
                return a + b;
            case '-':
                if(a - b > 0) return a - b;
                break;
            case '*':
                return a * b;
            case '/':
                if(a % b == 0) return a / b;
                break;
            default:
                throw new QuestionCreationException("Operator: " + op + " was not found.");
        }
        return null;
    }

    private void saveQuestion(String quest, Integer answer, String solution, Level level) {
        Question question = new Question();
        question.setQuestion(quest);
        question.setAnswer(answer);
        question.setSolution(solution);
        question.setLevel(level);
        questionService.create(question);
    }

    private void addCalculation(MathQuestionDTO question) {
        Integer a;

        if (question.getAnswer() == null) {
            a  = getRandomInteger(question.getNumbers());
            question.getNumbers().add(a);
        } else {
            a = question.getAnswer();
        }

        char op = getRandomOperator();
        Integer b = getRandomInteger(question.getNumbers());

        Integer calcRes;

        int calcCounter = 0;

        // while operation is not valid
        while ((op == '/' && b.equals(1)) || (calcRes = performOperation(op, a, b)) == null) {
            b = getRandomInteger(question.getNumbers());
            calcCounter++;
            if(calcCounter == 10) { // FIXME: hardcoded value for now
                calcCounter = 0;
                op = getRandomOperator(); // TODO getOperationExcept current operation
            }
        }

        question.setAnswer(calcRes);
        question.getNumbers().add(b);
        question.getSolution().append(",").append(a).append(op).append(b).append("=").append(calcRes);
    }

    @Override
    @Transactional
    public ApiResponseDTO generateMathQuestions(Integer levelNumber) {
        Level level = levelService.getByLevel(levelNumber);
        for(int i = 0; i < mathGameLimitProperties.getQuestionsNum(); i++) {
            StringBuilder solution = new StringBuilder();
            MathQuestionDTO question = new MathQuestionDTO();
            question.setSolution(solution);

            for (int j = 0; j < mathGameLimitProperties.getNumPerQuestion(); j++) {
                addCalculation(question);
            }

            Integer answer = question.getAnswer();
            if (!answerMatchesLevel(answer, level)) {
                --i;
                continue;
            }
            Collections.shuffle(question.getNumbers());

            String strNumbers = question.getNumbers().stream().map(Object::toString).collect(Collectors.joining(","));
            saveQuestion(strNumbers, answer, solution.substring(1), level);
        }

        return new ApiResponseDTO(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(),
                mathGameLimitProperties.getQuestionsNum() + " questions generated successfully.");
    }

    private boolean answerMatchesLevel(Integer answer, Level level) {
        Integer levelNumber = level.getLevel();
        if (levelNumber == 1) return (answer >= 0 && answer <= 99);
        else if (levelNumber == 2) return (answer > 99);
        return false;
    }

}
