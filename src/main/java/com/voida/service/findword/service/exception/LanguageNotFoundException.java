package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class LanguageNotFoundException extends RuntimeException {
    public LanguageNotFoundException(String s) {
        super(s);
    }
}
