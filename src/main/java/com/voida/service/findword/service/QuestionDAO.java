package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.entity.Question;

import java.util.List;

public interface QuestionDAO {
    List<Question> getAllByLevel(Integer level);
    List<Question> getAllByLanguageIdAndLevel(Integer languageId, Integer level);
    Boolean questionExists(String question);
    void create(Question question);
}
