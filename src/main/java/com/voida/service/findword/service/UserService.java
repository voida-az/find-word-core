package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.entity.User;

public interface UserService {
    User createIfNotExists(Long accountId);
}
