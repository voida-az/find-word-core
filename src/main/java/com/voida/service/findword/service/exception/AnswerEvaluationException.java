package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class AnswerEvaluationException extends RuntimeException {
    public AnswerEvaluationException(String message) {
        super(message);
    }
}
