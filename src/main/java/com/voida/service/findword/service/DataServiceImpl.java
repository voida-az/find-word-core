package com.voida.service.findword.service;

import com.voida.service.findword.web.dto.ApiResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.CacheManager;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class DataServiceImpl implements DataService {
    private final CacheManager cacheManager;

    @Override
//    @CacheEvict(value = "${value}", allEntries = true)
    public ApiResponseDTO evictCache(String value) {
        if(!cacheManager.getCacheNames().contains(value))
            return new ApiResponseDTO(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.getReasonPhrase(),
                    "Caches for value: " + value + " don't exist. Existing cache values: " + cacheManager.getCacheNames());
        Objects.requireNonNull(cacheManager.getCache(value)).clear();
        return new ApiResponseDTO(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "Data for: " + value + " refreshed successfully.");
    }

    @Override
    public ApiResponseDTO evictAllCaches() {
        cacheManager.getCacheNames()
                .forEach(cacheName -> Objects.requireNonNull(cacheManager.getCache(cacheName)).clear());
        return new ApiResponseDTO(HttpStatus.OK.value(), HttpStatus.OK.getReasonPhrase(), "All caches evicted successfully.");
    }

}
