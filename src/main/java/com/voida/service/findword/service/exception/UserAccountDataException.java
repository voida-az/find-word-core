package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class UserAccountDataException extends RuntimeException {
    public UserAccountDataException(String s) {
        super(s);
    }
}
