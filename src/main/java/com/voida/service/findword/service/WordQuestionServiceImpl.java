package com.voida.service.findword.service;

import com.voida.service.findword.config.WordLimitProperties;
import com.voida.service.findword.persistence.model.AnswerResult;
import com.voida.service.findword.persistence.model.entity.Question;
import com.voida.service.findword.persistence.model.RandomQuestion;
import com.voida.service.findword.persistence.model.entity.UserQuestion;
import com.voida.service.findword.service.exception.QuestionMismatchException;
import com.voida.service.findword.service.exception.QuestionNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
@RequiredArgsConstructor
@Profile({"wordGameDev", "wordGameTest", "wordGameProd"})
public class WordQuestionServiceImpl implements QuestionService {
    private final WordLimitProperties wordLimitProperties;
    private final QuestionDAO questionDAO;

    private String shuffleWord(String word) {
        List<String> letters = Arrays.asList(word.split(""));
        Collections.shuffle(letters);
        StringBuilder shuffledWord = new StringBuilder();
        for (String letter : letters) {
            shuffledWord.append(letter);
        }
        return shuffledWord.toString();
    }

    @Override
    @Cacheable("questions")
    public List<Question> getQuestions(Integer languageId, Integer level) {
        List<Question> allQuestions = questionDAO.getAllByLanguageIdAndLevel(languageId, level);
        // TODO: need to check in db instead of here, but in db some characters are count as 2 and we need
        //  it quickly, so added here for now, must remove later!
        List<Question> questions = new ArrayList<>();
        for(Question question: allQuestions) {
            if(question.getQuestion().length() >= wordLimitProperties.getMinLength() &&
                    question.getQuestion().length() <= wordLimitProperties.getMaxLength()) questions.add(question);
        }

        if(questions.size() == 0) throw new QuestionNotFoundException("No questions in database for level: " + level
                + " and languageId: " + languageId + ".");
        return questions;
    }

    @Override
    public RandomQuestion getRandomQuestion(List<Question> questions, List<Integer> userQuestionIds) {
        Question question = questions.get(new Random().nextInt(questions.size()));
        while (userQuestionIds.contains(question.getId())) {
            question = questions.get(new Random().nextInt(questions.size()));
        }
        String correctWord = question.getQuestion(), shuffledWord = correctWord;
        while(correctWord.equals(shuffledWord)) {
            shuffledWord = shuffleWord(correctWord);
        }
        return new RandomQuestion(question, shuffledWord, correctWord);
    }

    private boolean checkWord(String userAnswer, String correctAns) {
        if(userAnswer.length() > correctAns.length())
            throw new QuestionMismatchException("Word is too long, you didn't have enough letters to built it.");

        correctAns = correctAns.toUpperCase();
        for(int i = 0; i < userAnswer.length(); i++) {
            if(correctAns.indexOf(userAnswer.charAt(i)) == -1)
                throw new QuestionMismatchException("Word you entered is not built with given letters.");
        }

        return userAnswer.equals(correctAns) || questionDAO.questionExists(userAnswer);
    }

    @Override
    public AnswerResult checkAnswer(UserQuestion userQuestion, String userAnswer) {
        String correctAns = userQuestion.getQuestion().getQuestion();
        Integer diff = correctAns.length() - userAnswer.length();
        if(!checkWord(userAnswer, correctAns)) return new AnswerResult(diff, false, correctAns);
        return new AnswerResult(diff, true, correctAns);
    }

}
