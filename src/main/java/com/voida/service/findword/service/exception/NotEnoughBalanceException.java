package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class NotEnoughBalanceException extends CustomException {
    public NotEnoughBalanceException(Integer code, String message) {
        super(code, message);
    }
}
