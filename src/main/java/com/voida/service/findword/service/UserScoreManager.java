package com.voida.service.findword.service;


import com.voida.service.findword.web.dto.HistoryDTO;
import com.voida.service.findword.web.dto.LeaderBoardDTO;
import com.voida.service.findword.web.dto.StatisticsDTO;

public interface UserScoreManager {
    LeaderBoardDTO getLeaderBoard(Long accountId, String appKeyword, Integer periodId);
    StatisticsDTO getStatistics(Long accountId);
    HistoryDTO getHistory(Long accountId, Integer page);
}
