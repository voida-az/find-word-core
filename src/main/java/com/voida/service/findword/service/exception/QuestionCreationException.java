package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class QuestionCreationException extends RuntimeException {
    public QuestionCreationException(String s) {
        super(s);
    }
}
