package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class LevelNotFoundException extends RuntimeException {
    public LevelNotFoundException(String s) {
        super(s);
    }
}
