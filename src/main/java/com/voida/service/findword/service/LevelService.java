package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.entity.Level;

public interface LevelService {
    Level getByLevel(Integer level);
}
