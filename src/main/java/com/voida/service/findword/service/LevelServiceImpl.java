package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.entity.Level;
import com.voida.service.findword.persistence.repository.LevelRepository;
import com.voida.service.findword.service.exception.LevelNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LevelServiceImpl implements LevelService {
    private final LevelRepository levelRepository;

    @Override
    public Level getByLevel(Integer level) {
        return levelRepository.findByLevel(level)
                .orElseThrow(() -> new LevelNotFoundException("Level: " + level + " was not found."));
    }

}
