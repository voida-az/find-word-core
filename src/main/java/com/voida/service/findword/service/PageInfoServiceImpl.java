package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.entity.PageInfo;
import com.voida.service.findword.persistence.repository.PageInfoRepository;
import com.voida.service.findword.service.exception.PageInfoNotFoundException;
import com.voida.service.findword.web.dto.PageInfoDTO;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PageInfoServiceImpl implements PageInfoService {
    private final PageInfoRepository pageInfoRepository;
    private final LanguageService languageService;
    private final ModelMapper modelMapper;

    @Override
    @Cacheable("page_info")
    public PageInfoDTO getByPageIdAndLanguageId(Integer pageId, Integer languageId) {
        if(languageId == null) languageId = languageService.getDefault().getId();
        Integer finalLanguageId = languageId;
        PageInfo pageInfo = pageInfoRepository.findByPageIdAndLanguageId(pageId, languageId)
                .orElseThrow(() -> new PageInfoNotFoundException("PageInfo for pageId: "
                        + pageId + " and languageId: " + finalLanguageId + " was not found."));
        return modelMapper.map(pageInfo, PageInfoDTO.class);
    }

}
