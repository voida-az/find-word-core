package com.voida.service.findword.service;

import com.voida.service.findword.config.LimitProperties;
import com.voida.service.findword.persistence.model.entity.User;
import com.voida.service.findword.persistence.model.entity.UserGame;
import com.voida.service.findword.persistence.repository.UserGameRepository;
import com.voida.service.findword.service.exception.UserGameCreationException;
import com.voida.service.findword.service.exception.QuestionLimitExceededException;
import com.voida.service.findword.web.dto.GameDTO;
import com.voida.service.findword.web.dto.LeaderScore;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserGameServiceImpl implements UserGameService {
    private final UserGameRepository userGameRepository;
    private final DateTimeService dateTimeService;
    private final LimitProperties limitProperties;

    @Value("${history.page.size}")
    private Integer historyPageSize;

    @Override
    public UserGame getCurrentGame(Long accountId) {
        Date date = dateTimeService.getStartOfDay();
        List<UserGame> userGames = userGameRepository.findLastByAccountId(PageRequest.of(0, 1), accountId, date); //size will be 0 or 1
        if(userGames.size() == 0) return null;
        return userGames.get(0);
    }

    private UserGame saveUserGame(UserGame userGame) {
        try {
            return userGameRepository.save(userGame);
        } catch (Throwable t) {
            throw new UserGameCreationException(t.getMessage());
        }
    }

    @Override
    public UserGame create(User user) {
        UserGame userGame = new UserGame();
        userGame.setUser(user);
        userGame.setDate(Calendar.getInstance().getTime());
        userGame.setExpirationDate(dateTimeService.getEndOfDay());
        userGame.setQuestionPlayed(0);
        userGame.setHintsPlayed(0);
        userGame.setPoints(0);
        userGame.setSpentTime((long) 0);
        userGame.setQuestionNum(limitProperties.getQuestionsNumPerGame());
        return saveUserGame(userGame);
    }

    @Override
    public void updatePlayedWords(UserGame userGame) {
        userGame.setQuestionPlayed(userGame.getQuestionPlayed() + 1);
        saveUserGame(userGame);
    }

    @Override
    public void updatePlayedHints(UserGame userGame) {
        if(userGame.getHintsPlayed() >= limitProperties.getHintsNumPerGame()) throw new QuestionLimitExceededException("You have " +
                "reached your daily hint limit: " + limitProperties.getHintsNumPerGame() + " hints for this game.");
        userGame.setHintsPlayed(userGame.getHintsPlayed() + 1);
        saveUserGame(userGame);
    }

    @Override
    public List<UserGame> getByAccountId(Long accountId) {
        return userGameRepository.findByAccountId(accountId);
    }

    @Override
    public List<UserGame> getByAccountIdAndPage(Long accountId, Integer page) {
        if(page == null) return userGameRepository.findByAccountId(accountId);
        return userGameRepository.findByAccountIdAndPage(PageRequest.of(page - 1, historyPageSize), accountId);
    }

    @Override
    public GameDTO getGameDTO(UserGame userGame) {
        GameDTO gameDTO = new GameDTO();
        gameDTO.setExpirationDate(dateTimeService.formatDate(userGame.getExpirationDate()));
        gameDTO.setCreationDate(dateTimeService.formatDate(userGame.getDate()));
        gameDTO.setPoints(userGame.getPoints());
        gameDTO.setTotalQuestions(userGame.getQuestionNum());
        gameDTO.setPlayedQuestions(userGame.getQuestionPlayed());
        gameDTO.setRemainingQuestions(userGame.getQuestionNum() - userGame.getQuestionPlayed());
        return gameDTO;
    }

    @Override
    public void updateGameScore(UserGame userGame, Integer points, Long spentTime) {
        userGame.setPoints(userGame.getPoints() + points);
        userGame.setSpentTime(userGame.getSpentTime() + spentTime);
        try {
            userGameRepository.save(userGame);
        } catch (Throwable t) {
            throw new UserGameCreationException(t.getMessage());
        }
    }

    @Override //TODO: delete
    public List<LeaderScore> findLeadersByPeriod(Integer userNum, Date startDate) {
        return userGameRepository.findLeadersInPeriod(PageRequest.of(0, userNum), startDate);
    }

    @Override //TODO: delete
    public LeaderScore getUserPoints(Long accountId, Date startDate) {
        LeaderScore leaderScore = userGameRepository.findUserPointsInPeriod(accountId, startDate);
        if(leaderScore.getSpentTime() == null) return new LeaderScore(accountId, (long)0, (long)0);
        return leaderScore;
    }

    @Override //TODO: delete
    public Integer getUserPosition(Long accountId, Long points, Date startDate, Long spentTime) {
        List<Integer> userPosition = userGameRepository.findUserPosition(accountId, points, startDate, spentTime);
        if(userPosition == null) return 0;
        return userPosition.size();
    }

}
