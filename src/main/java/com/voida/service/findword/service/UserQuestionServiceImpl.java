package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.entity.UserGame;
import com.voida.service.findword.persistence.model.entity.UserQuestion;
import com.voida.service.findword.persistence.model.entity.Question;
import com.voida.service.findword.persistence.repository.UserQuestionRepository;
import com.voida.service.findword.service.exception.UserQuestionCreationException;
import com.voida.service.findword.service.exception.QuestionNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserQuestionServiceImpl implements UserQuestionService {
    private final UserQuestionRepository userQuestionRepository;
    private final DateTimeService dateTimeService;

    private void saveUserQuestion(UserQuestion userQuestion) {
        try {
            userQuestionRepository.save(userQuestion);
        } catch (Throwable t) {
            throw new UserQuestionCreationException(t.getMessage());
        }
    }

    @Override
    public void create(UserGame userGame, Question question) {
        UserQuestion userQuestion = new UserQuestion();
        userQuestion.setUserGame(userGame);
        userQuestion.setQuestion(question);
        userQuestion.setDate(Calendar.getInstance().getTime());
        userQuestion.setPoints(0);
        userQuestion.setSpentTime((long) 0);
        saveUserQuestion(userQuestion);
    }

    @Override
    public List<UserQuestion> getByAccountId(Long accountId) {
        return userQuestionRepository.findByAccountId(accountId);
    }

    @Override
    public void setUserAns(UserQuestion userQuestion, String answer, Integer points, Long spentTime) {
        userQuestion.setAnswer(answer);
        userQuestion.setPoints(points);
        userQuestion.setSpentTime(userQuestion.getSpentTime() + spentTime);
        saveUserQuestion(userQuestion);
    }

//    @Override
//    @Transactional
//    public void updateUnansweredQuestions(Date startDate, Date endDate, Long spentTime) {
//        userQuestionRepository.updateUnansweredQuestions(startDate, endDate, spentTime);
//    }

    @Override
    public List<UserQuestion> getLastDayByUserId(Integer userId) {
        Date date = dateTimeService.getStartOfDay();
        return userQuestionRepository.findByDateAndUserId(date, userId);
    }

    @Override
    public UserQuestion getLatestByAccountId(Long accountId) {
        Date date = dateTimeService.getStartOfDay();
        List<UserQuestion> userQuestions = userQuestionRepository.findLastByAccountId(PageRequest.of(0, 1), date, accountId);
        if(userQuestions.size() == 0) throw new QuestionNotFoundException("No words found to check for user with accountId: " + accountId + ".");
        return userQuestions.get(0);
    }

}
