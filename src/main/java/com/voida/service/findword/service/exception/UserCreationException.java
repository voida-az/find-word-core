package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class UserCreationException extends RuntimeException {
    public UserCreationException(String message) {
        super(message);
    }
}
