package com.voida.service.findword.service;

import com.voida.service.findword.web.dto.ApiResponseDTO;

public interface DataService {
    ApiResponseDTO evictCache(String value);
    ApiResponseDTO evictAllCaches();
}
