package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.entity.UserGame;
import com.voida.service.findword.persistence.model.entity.UserQuestion;
import com.voida.service.findword.persistence.model.entity.Question;

import java.util.List;

public interface UserQuestionService {
    void create(UserGame userGame, Question question);
    List<UserQuestion> getByAccountId(Long accountId);
    List<UserQuestion> getLastDayByUserId(Integer userId);
    UserQuestion getLatestByAccountId(Long accountId);
    void setUserAns(UserQuestion userQuestion, String answer, Integer points, Long spentTime);
//    void updateUnansweredQuestions(Date startDate, Date endDate, Long spentTime);
}
