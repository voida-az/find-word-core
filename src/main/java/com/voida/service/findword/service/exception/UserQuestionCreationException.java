package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class UserQuestionCreationException extends RuntimeException {
    public UserQuestionCreationException(String s) {
        super(s);
    }
}
