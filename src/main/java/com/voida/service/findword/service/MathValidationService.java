package com.voida.service.findword.service;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Profile({"mathGameDev", "mathGameTest", "mathGameProd"})
public class MathValidationService implements ValidationService {

    @Override
    public boolean isValid(String answer) {
        return answer != null && answer.matches("^[0-9+*-/() ]+");
    }

}
