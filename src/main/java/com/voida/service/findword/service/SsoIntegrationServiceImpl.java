package com.voida.service.findword.service;

import com.voida.logging.Audited;
import com.voida.service.findword.config.SsoApiPathProperties;
import com.voida.service.findword.persistence.model.sso.ChargeResponse;
import com.voida.service.findword.persistence.model.sso.UserAccountDataResponse;
import com.voida.service.findword.persistence.model.sso.VerifyTokenResponse;
import com.voida.service.findword.service.exception.NotEnoughBalanceException;
import com.voida.service.findword.service.exception.SubscriptionChargeException;
import com.voida.service.findword.service.exception.TokenVerificationException;
import com.voida.service.findword.service.exception.UserAccountDataException;
import com.voida.service.findword.web.dto.ChargeResponseDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@RequiredArgsConstructor
@Log4j2
public class SsoIntegrationServiceImpl implements SsoIntegrationService {
    private final SsoApiPathProperties ssoApiPathProperties;


    @Value("${service.config.tariff_id}")
    private Integer tariffId;

    @Override
    public VerifyTokenResponse verifyToken(String keyword, String token) {
        VerifyTokenResponse response;
        try {
            String url = ssoApiPathProperties.getVerifyToken() + "/" + keyword + "/" + token;
//            log.debug("Verify token request: [GET: {}]", url);
            response = new RestTemplate().getForObject(url, VerifyTokenResponse.class);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new TokenVerificationException(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Token: " + token + " verification with sso failed.");
        }

        if(response == null) throw new TokenVerificationException(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                "Token: " + token + " verification with sso failed, SSO response was null.");

        Integer statusCode = response.getCode();

        if(statusCode != HttpStatus.OK.value()) {
            String message = "Token: " + token + " verification with sso failed, " + response.getMessage() + ".";
            if (HttpStatus.CONFLICT.value() == statusCode) throw new NotEnoughBalanceException(statusCode, message);
            else throw new TokenVerificationException(statusCode, message);
        }

        return response;
    }

    @Audited
    @Override
    public ChargeResponseDTO directCharge(String keyword, Long accountId) {
        ChargeResponse response;
        try {
            response = new RestTemplate().postForObject(ssoApiPathProperties.getDirectCharging() + "/" + keyword + "/" + accountId + "/"
                    + tariffId, null, ChargeResponse.class);
            if(response == null) throw new SubscriptionChargeException("Charge response was null.");
            if(!response.getCode().equals(HttpStatus.OK.value()))
                return new ChargeResponseDTO(false, response.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new SubscriptionChargeException("Extra game purchase for accountId:" + accountId + " failed with sso.");
        }
        return new ChargeResponseDTO(true, response.getMessage());
    }

    @Override
    @Audited
    public List<UserAccountDataResponse.UserAccountData> getUserAccounts(List<Long> accountIdList, String appKeyword) {
        UserAccountDataResponse response;
        try {
            String url = ssoApiPathProperties.getUserAccountData() + "/" + appKeyword + "?ids=" + StringUtils.join(accountIdList, ',');
            response = new RestTemplate().getForObject(url, UserAccountDataResponse.class);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new UserAccountDataException("Getting user accounts data for accountIds: " + accountIdList.toString() + " failed.");
        }

        if(response == null || response.getCode() != HttpStatus.OK.value()) {
            log.error("SSO response: " + response);
            throw new UserAccountDataException("Getting user accounts data for accountIds: " + accountIdList.toString() + " failed.");
        }
        return response.getAccounts();
    }

}
