package com.voida.service.findword.service;

import com.voida.service.findword.web.dto.PrizesDTO;

public interface PrizeService {
    PrizesDTO getAllByPeriodId(Integer periodId);
}
