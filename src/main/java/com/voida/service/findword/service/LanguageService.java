package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.entity.Language;

public interface LanguageService {
    Language getDefault();
}
