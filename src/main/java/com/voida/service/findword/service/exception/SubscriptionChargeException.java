package com.voida.service.findword.service.exception;

import com.voida.logging.NoStackTrace;

@NoStackTrace
public class SubscriptionChargeException extends RuntimeException {
    public SubscriptionChargeException(String message) {
        super(message);
    }
}
