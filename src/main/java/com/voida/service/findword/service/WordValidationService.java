package com.voida.service.findword.service;

import com.voida.service.findword.config.WordLimitProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Profile({"wordGameDev", "wordGameTest", "wordGameProd"})
public class WordValidationService implements ValidationService {
    private final WordLimitProperties wordLimitProperties;

    @Override
    public boolean isValid(String answer) {
        return answer != null &&
                answer.length() >= wordLimitProperties.getMinLength() &&
                answer.length() <= wordLimitProperties.getMaxLength();
    }

}
