package com.voida.service.findword.service;

import com.voida.service.findword.persistence.model.entity.User;
import com.voida.service.findword.persistence.model.entity.UserScore;
import com.voida.service.findword.persistence.repository.UserScoreRepository;
import com.voida.service.findword.service.exception.UserScoreCreationException;
import com.voida.service.findword.service.exception.UserScoreNotFoundException;
import com.voida.service.findword.service.exception.UserScoreUpdateException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class UserScoreServiceImpl implements UserScoreService {
    private final UserScoreRepository userScoreRepository;

    @Override
    public UserScore getUserScore(Long accountId) {
        return userScoreRepository.findByAccountId(accountId)
                .orElseThrow(() -> new UserScoreNotFoundException("UserScore for user with accountId: " + accountId + " was not found."));
    }

    @Override
    @Transactional
    public void updateUserScore(Integer userId, Integer points) {
        try {
            userScoreRepository.updateUserScore(userId, Long.valueOf(points));
        } catch (Throwable t) {
            throw new UserScoreUpdateException(t.getMessage());
        }
    }

    @Override
    public void create(User user) {
        UserScore userScore = new UserScore();
        userScore.setUser(user);
        userScore.setPoints((long) 0);

        try {
            userScoreRepository.save(userScore);
        } catch (Throwable t) {
            throw new UserScoreCreationException(t.getMessage());
        }
    }

}
