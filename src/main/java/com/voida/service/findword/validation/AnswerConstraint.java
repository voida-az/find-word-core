package com.voida.service.findword.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = AnswerValidator.class)
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface AnswerConstraint {
    String message() default "Invalid answer.";
    Class<?>[] groups() default {}; // ignore warning, don't delete these fields
    Class<? extends Payload>[] payload() default {};
}
